# SR Log Parser

## Changelog

2023-08-06 - initial version - Randomized

## Disclaimer

As a sysadmin, i am not responsible for anything which might happen when using this code.

## Licence

[WTFPL](https://en.wikipedia.org/wiki/WTFPL)

Do whatever you want with this.

## What is it

Parse a Star Realms log and gather some stats about your game

## Installation

Just drop cards.csv and parse.php in any directory served by any decent webserver with php support


## Notes

cards.csv created from [google doc](https://docs.google.com/spreadsheets/d/1IePxyWlr6AEI0uwGhos1a6e6S84W0ssHwt-2e6AZ5Aw/edit#gid=736622417) minus columns "text" and "notes"

