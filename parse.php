<?php


if( ! isset( $_SERVER["HTTP_HOST"])) {
   parse_str($argv[1], $_POST);
}
if( $_POST["called"] != 1 ) {
   print "Copy SR log below" ;
   $self = "$_SERVER[PHP_SELF]";
   $form= <<<EOF
<form method="POST" action="$self">
<textarea  style="width:200px; height:600px;" name="log">Paste log Here</textarea>
<input type="submit" name="Ok"/>
<input type="hidden" name="called" value="1"/>
</form>
EOF;
print $form ;


}
else {
   $ext=array() ;
   $usedExt=array() ;
   $cards=array() ;
   $csvfile = fopen('cards.csv', "r") or die( "unable to open card file" ) ;
   while( $line = fgetcsv( $csvfile ) ){
      array_push( $ext, $line[0] );
      array_push( $cards, $line[2] ) ;
   }

   $turns = 0 ;
   $winner = "" ;
   $opp = "";
   $first = "" ;
   $buys = array() ;
   $data = htmlspecialchars(trim( $_POST["log"] ) ) ;
   $data_ar = preg_split("/\r\n|\n|\r/", $data);

   foreach ($data_ar as $line ) {
      if( strpos( $line, "ends turn" ) ) {
         static $turns ;
         $turns++ ;
      }

      if ( preg_match(  '/^([a-zA-Z0-9]+) ends turn 1$/', $line, $match ) ) {
         $first = $match[1] ;
      }

      if ( preg_match(  '/^([a-zA-Z0-9]+) ends turn 2$/', $line, $match ) ) {
         $opp = $match[1] ;
      }

      if( preg_match( '/^Played &lt;color=#[A-Z0-9]+&gt;(.*)&lt;\/color&gt;/', $line, $match ) ) {
         guessExpansion( $match[1], $cards, $ext ) ;
      }

      if ( strpos( $line, "===" ) ) {
         $line_a = explode( " ",  "$line" ) ;
         $winner = $line_a[9] ;
      }

      if( preg_match( '/^Acquired &lt;color=#[A-Z0-9]+&gt;(.*)&lt;\/color&gt;/', $line, $match ) ) {
         $acLine = explode( " ", ltrim($previousLine ) ) ;
         $p=$acLine[0] ;
         if(  ( strcmp( "$p", "$opp" ) == 0 ) or ( strcmp( $p, $first ) ) == 0)  {
            if( ! isset( $buys[$p] ) ){
               $buys[$p] = 1 ;
            }else {
               $buys[$p] ++ ;
            }
         }
      }

   $previousLine=$line ;
   }
   echo "<br />" ;
   echo "\n";
   echo "\n";
   echo "\n";

   $result = array(
      "first player" => $first,
      "winner" => $winner,
      "opp" => $opp,
      "1st_player_bought_cards" => $buys[$first],
      "opp_bought_cards" => $buys[$opp],
      "num_turns" => $turns ,
      "expansions" =>  array_values( array_unique($usedExt) ),
      ) ;
   print json_encode( $result, JSON_PRETTY_PRINT );
   print "<p>in csv format:</p>" ;
   print "1st player, winner, winner_bought_cards, opponent, opponent_bought_cards, #turns, expansions<br />" ;
   print "$first,$winner, $buys[$first], $opp, $buys[$opp], $turns, " . implode( " ", array_values( array_unique($usedExt) ) );

}
function guessExpansion( $card, $cards, $ext ) {
   global $usedExt ;
   if( $key = array_search( $card, $cards)){
      array_push( $usedExt, $ext[$key] ) ;
   }
}
